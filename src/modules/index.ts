import api from '@/modules/api'
import io from '@/modules/io'
import ipc from '@/modules/ipc'
import psdeamon from '@/modules/psdaemon'
import tokenScanner from '@/modules/token_scanner'

type ModuleInit = () => void | Promise<void>

export const modules: ModuleInit[] = [api, io, ipc, psdeamon, tokenScanner]

export default function (): void {
  for (const mod of modules) {
    Promise.resolve(mod()).catch(console.error.bind(console))
  }
}
