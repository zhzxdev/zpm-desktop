import logger from 'electron-log'
import * as wm from '@/wm'
import { app } from 'electron'
import { autoUpdater } from 'electron-updater'
import { writeFile } from 'fs-extra'
import { Manager } from 'socket.io-client'
import { runInNewContext } from 'vm'
import { RPCEndpoint } from '@/misc/rpc'
import { APP_VER, execAsync, prepareQuit, tmpFileAsync } from '@/misc/utils'
import { screenshot } from '@/misc/screenshot'
import { exec } from 'child_process'
import { setPSDEnabled } from '@/modules/psdaemon'

let online = false
const pin = {}

export default function (): void {
  const manager = new Manager(ZPM_SERVER, { rejectUnauthorized: false })
  const socket = manager.socket('/device')
  const rpc = new RPCEndpoint((msg) => socket.emit('rpc', msg))
  socket.on('rpc', rpc.recv.bind(rpc))

  socket.on('connect', () => {
    logger.log(`[IO] socket ${socket.id} connected`)
    online = true
  })

  socket.on('connect_error', (e: Error) => {
    logger.error(`[IO] error: ${e.message}`)
    setTimeout(() => {
      socket.connect()
    }, 1000)
  })

  socket.on('disconnect', () => {
    logger.warn(`[IO] socket ${socket.id} disconnected`)
    online = false
  })

  rpc.handle('ping', () => {
    return {
      ok: 1,
      ts: Date.now(),
      ver: APP_VER
    }
  })

  rpc.handle('quit', async () => {
    await prepareQuit()
    app.quit()
  })

  rpc.handle('relaunch', async () => {
    await prepareQuit()
    app.relaunch()
    // https://stackoverflow.com/questions/38540903/app-relaunchoptions-is-not-working-in-electron
    app.quit()
  })

  rpc.handle('killps', async () => {
    try {
      await execAsync('taskkill /f /im printstation.exe')
      return true
    } catch (e) {
      return false
    }
  })

  rpc.handle('startexplorer', async () => {
    try {
      await execAsync('start C:\\Windows\\explorer.exe')
      return true
    } catch (e) {
      return false
    }
  })

  rpc.handle(
    'exec',
    ({ command }) =>
      new Promise((resolve) => {
        exec(command, { timeout: 45 * 1000 }, (err, stdout, stderr) => {
          const result: any = { stdout, stderr }
          if (err?.code) result.code = err.code
          if (err?.signal) result.signal = err.signal
          resolve(result)
        })
      })
  )

  rpc.handle('execbat', async ({ code }) => {
    const { path, fd, remove } = await tmpFileAsync({ postfix: '.bat' })
    await writeFile(fd, code)

    const result = await new Promise((resolve) => {
      exec(path, { timeout: 45 * 1000 }, (err, stdout, stderr) => {
        const result: any = { stdout, stderr }
        if (err?.code) result.code = err.code
        if (err?.signal) result.signal = err.signal
        resolve(result)
      })
    })

    remove()
    return result
  })

  rpc.handle('screenshot', async () => {
    const buf = await screenshot()
    return buf.toString('base64')
  })

  rpc.handle('eval', async ({ code }) => {
    const ctx = {
      require: eval('require'),
      process: eval('process'),
      zpm: {
        ver: APP_VER
      },
      pin,
      wm,
      logger
    }

    code = `(async()=>{${code}})()`
    const result = await runInNewContext(code, ctx)

    if (result === undefined) return
    return JSON.parse(JSON.stringify(result))
  })

  rpc.handle('setpsd', async ({ enabled }) => {
    setPSDEnabled(enabled)
    return enabled
  })

  rpc.handle('appupgrade', async () => {
    const current = autoUpdater.currentVersion.version
    const result = await autoUpdater.checkForUpdates()
    if (result.downloadPromise) {
      result.downloadPromise
        .then(async () => {
          await prepareQuit()
          autoUpdater.quitAndInstall(true, true)
        })
        .catch((e) => {
          logger.error(`[UPDATER] Error: ${e.message}`)
        })
      return `Update from ${current} to ${result.updateInfo.version}`
    }
    return `No update avaliable (current=${current})`
  })

  rpc.handle('setfloaticon', async ({ enabled }) => {
    enabled ? wm.showFloatIcon() : wm.hideFloatIcon()
  })

  rpc.handle('closewindows', async () => {
    wm.getWindows()
      .filter((win) => !wm.getWindowMeta(win).isFloatIcon)
      .forEach((win) => {
        win.setClosable(true)
        win.close()
      })
  })
}

export function isOnline(): boolean {
  return online
}
