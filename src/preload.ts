import logger from 'electron-log'
import { autoUpdater } from 'electron-updater'

if (process.env.NODE_ENV === 'development') {
  // Disable log files while developing
  logger.transports.file.level = false
} else {
  logger.transports.file.level = 'info'
}

autoUpdater.logger = logger
