import cp from 'child_process'
import pkg from '@/../package.json'
import tmp from 'tmp'
import { promisify } from 'util'
import { getWindows } from '@/wm'

export const APP_VER = pkg.version

export function wait(ms: number): Promise<void> {
  return new Promise<void>((resolve) => setTimeout(resolve, ms))
}

export const execAsync = promisify(cp.exec)

export async function run(cmd: string): Promise<string> {
  const { stdout } = await execAsync(cmd)
  return stdout.trim()
}

export async function listDrives(): Promise<string[]> {
  const output = await run('wmic logicaldisk get name')
  return output
    .split('\r\r\n')
    .filter((value) => /[A-Za-z]:/.test(value))
    .map((value) => value.trim())
}

export interface IProcessInfo {
  CommandLine: string
  ExecutablePath: string
}

export async function getProcessInfo(exeName: string): Promise<IProcessInfo[]> {
  const output = await run(`wmic path win32_process where "name='${exeName}'" get CommandLine,ExecutablePath /format:list`)
  const result = output
    .split('\r\r\n\r\r\n')
    .map((x) => x.trim())
    .filter((x) => x)
    .map((x) => {
      const y: any = {}
      x.split('\r\r\n')
        .map((y) => y.trim())
        .filter((y) => y)
        .forEach((pair) => {
          const i = pair.indexOf('=')
          y[pair.substring(0, i)] = pair.substring(i + 1)
        })
      return y
    })
  return result
}

export async function prepareQuit(): Promise<void> {
  const windows = getWindows()
  windows.forEach((win) => win.setClosable(true))
}

export function tmpFileAsync(options: tmp.FileOptions): Promise<{ path: string; fd: number; remove: () => void }> {
  return new Promise((resolve, reject) => {
    tmp.file(options, (err, path, fd, remove) => {
      if (err) return reject(err)
      return resolve({ path, fd, remove })
    })
  })
}
