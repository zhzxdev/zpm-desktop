import logger from 'electron-log'
import { spawn } from 'child_process'
import { execAsync, getProcessInfo, wait } from '@/misc/utils'
import { getWindowMeta, getWindows } from '@/wm'

const PROCESS_NAME = 'printstation.exe'
const TOKEN_SCANNER_INTERVAL = 1000
let cmd: string
let PSDEnabled = true

export default async function (): Promise<void> {
  for (;;) {
    if (PSDEnabled && !getWindows().some((win) => getWindowMeta(win).isLoggedIn)) {
      const info = await getProcessInfo(PROCESS_NAME)
      if (cmd) {
        if (!info.length) {
          logger.log(`[PS Daemon] Restarting`)
          const cp = spawn(cmd, { stdio: 'ignore', detached: true })
          cp.unref()
        }
        try {
          await execAsync('taskkill /f /im explorer.exe /im taskmgr.exe')
        } catch (e) {
          //
        }
      } else {
        if (info.length) {
          cmd = info[0].ExecutablePath
          logger.log(`[PS Daemon] Found cmd: ${cmd}`)
        }
      }
    }
    await wait(TOKEN_SCANNER_INTERVAL)
  }
}

export function setPSDEnabled(enabled: boolean): void {
  PSDEnabled = enabled
}
