/* eslint-disable @typescript-eslint/no-var-requires */
require('dotenv').config()
const { DefinePlugin } = require('webpack')

module.exports = {
  plugins: [
    new DefinePlugin({
      ZPM_SERVER: `'${process.env.ZPM_SERVER}'`,
      ZPM_UI: `'${process.env.ZPM_UI}'`
    })
  ]
}
