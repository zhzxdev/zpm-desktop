import { BrowserWindow } from 'electron'
import { APP_ICON_PATH } from '@/misc'

interface IWindowMeta {
  isLoggedIn?: boolean
  isFloatIcon?: boolean
}

const windows = new Map<number, BrowserWindow>()
const metas = new WeakMap<BrowserWindow, IWindowMeta>()
let floatIconId: number

export function createFloatIconWindow(): number {
  if (floatIconId) return floatIconId

  const win = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
      backgroundThrottling: false
    },
    frame: false,
    icon: APP_ICON_PATH,
    resizable: false,
    alwaysOnTop: true,
    closable: false,
    minimizable: false,
    maximizable: false,
    fullscreenable: false,
    skipTaskbar: true,
    width: 88,
    height: 36
  })
  metas.set(win, {
    isFloatIcon: true
  })
  const id = win.webContents.id
  win.setMenu(null)
  win.setAlwaysOnTop(true, 'pop-up-menu')

  void win.loadURL(ZPM_UI + '/floaticon/index.html')

  windows.set(id, win)
  win.on('closed', () => {
    floatIconId = 0
    windows.delete(id)
  })
  floatIconId = id
  return id
}

export function createWindow(): number {
  const win = new BrowserWindow({
    webPreferences: { nodeIntegration: true },
    frame: false,
    icon: APP_ICON_PATH
  })
  metas.set(win, {})
  const id = win.webContents.id
  win.setMenu(null)
  void win.loadURL(ZPM_UI)

  windows.set(id, win)
  win.on('closed', () => {
    windows.delete(id)
  })
  return id
}

export function closeWindow(handler: number): boolean {
  const win = windows.get(handler)
  if (!win) return false
  win.close()
  return true
}

export function getWindow(handler: number): BrowserWindow | undefined {
  return windows.get(handler)
}

export function getFloatIconWindow(): BrowserWindow | undefined {
  return getWindow(floatIconId)
}

export function getWindowMeta(win: BrowserWindow): IWindowMeta {
  return metas.get(win)!
}

export function getWindows(): BrowserWindow[] {
  return [...windows.values()]
}

export function showFloatIcon(): void {
  const win = getFloatIconWindow()
  win && win.show()
}

export function hideFloatIcon(): void {
  const win = getFloatIconWindow()
  win && win.hide()
}
