import path from 'path'

export const APP_ICON_PATH = path.join(__static, 'logo.png')
