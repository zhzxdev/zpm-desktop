import { Menu, Tray } from 'electron'
import { APP_VER } from '@/misc/utils'
import { createWindow } from '@/wm'
import { getCurrentUser } from '@/modules/token_scanner'
import { APP_ICON_PATH } from '@/misc'

let tray: Tray

export function createTray(): void {
  tray = new Tray(APP_ICON_PATH)
  const contextMenu = Menu.buildFromTemplate([{ label: `App version: ${APP_VER}` }])
  tray.setContextMenu(contextMenu)
  tray.on('double-click', () => {
    if (getCurrentUser()) {
      createWindow()
    }
  })
}
