import path from 'path'
import fs from 'fs-extra'
import tmp from 'tmp'
import { execAsync } from '@/misc/utils'

const exeDir = path.join(__static, 'screen_capture')
const exePath = path.join(exeDir, 'screen_capture.bat')

interface IScreenshotOptions {
  screen?: string
  format?: string
  filename?: string
}

export async function screenshot(options: IScreenshotOptions = {}): Promise<Buffer> {
  const displayName = options.screen
  const format = options.format || 'jpg'
  const tmpName = await new Promise<string>((resolve, reject) =>
    tmp.tmpName({ postfix: `.${format}` }, (err, name) => (err ? reject(err) : resolve(name)))
  )
  const displayChoice = displayName ? ` /d "${displayName}"` : ''
  await execAsync(`"${exePath}" "${tmpName}" ${displayChoice}`, { cwd: exeDir, windowsHide: true })
  const img = await fs.readFile(tmpName)
  await fs.unlink(tmpName)
  return img
}

interface IDisplayInfo {
  id: string
  name: string
  top: number
  right: number
  bottom: number
  left: number
  dpiScale: number
  height: number
  width: number
}

function parseDisplaysOutput(output: string): IDisplayInfo[] {
  const displaysStartPattern = /2>nul {2}\|\| /
  const { 0: match, index } = displaysStartPattern.exec(output)!
  return output
    .slice(index + match.length)
    .split('\n')
    .map((s) => s.replace(/[\n\r]/g, ''))
    .map((s) => s.match(/(.*?);(.?\d+);(.?\d+);(.?\d+);(.?\d+);(.?\d*\.?\d+)/)!)
    .filter((s) => s)
    .map((m) => ({ id: m[1], name: m[1], top: +m[2], right: +m[3], bottom: +m[4], left: +m[5], dpiScale: +m[6] }))
    .map((d) => Object.assign(d, { height: d.bottom - d.top, width: d.right - d.left }))
}

export async function listDisplays(): Promise<IDisplayInfo[]> {
  const { stdout } = await execAsync(`"${exePath}" /list`, { cwd: exeDir })
  return parseDisplaysOutput(stdout)
}
