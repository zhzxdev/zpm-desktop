import path from 'path'
import { app, Event, ipcMain, shell } from 'electron'
import { writeFile } from 'fs-extra'
import { getWindowMeta, getWindow } from '@/wm'
import { getCurrentUser } from '@/modules/token_scanner'
import { isOnline } from './io'

export default function (): void {
  if (process.env.NODE_ENV === 'development') {
    ipcMain.handle('openDevTools', (ev) => {
      ev.sender.openDevTools()
    })
  }

  const preventClose = (ev: Event) => {
    ev.preventDefault()
  }

  ipcMain.handle('enterLogin', (ev) => {
    const win = getWindow(ev.sender.id)!
    win.setFullScreen(true)
    win.setMinimizable(false)
    win.setClosable(false)
    win.setAlwaysOnTop(true, 'pop-up-menu')
    win.focus()
    win.on('close', preventClose)
  })

  ipcMain.handle('exitLogin', (ev) => {
    const win = getWindow(ev.sender.id)!
    win.off('close', preventClose)
    win.setAlwaysOnTop(false, 'pop-up-menu')
    win.setClosable(true)
    win.setMinimizable(true)
    win.setFullScreen(false)
    getWindowMeta(win).isLoggedIn = true
  })

  ipcMain.handle('saveToken', async (ev, token) => {
    const tokenPath = path.join(app.getPath('desktop'), 'zhzx-print-station')
    await writeFile(tokenPath, token)
    shell.showItemInFolder(tokenPath)
  })

  ipcMain.handle('minimize', (ev) => {
    const win = getWindow(ev.sender.id)!
    win.minimize()
  })

  ipcMain.handle('maximize', (ev) => {
    const win = getWindow(ev.sender.id)!
    win.isMaximized() ? win.restore() : win.maximize()
  })

  ipcMain.handle('close', (ev) => {
    const win = getWindow(ev.sender.id)!
    win.close()
  })

  ipcMain.handle('forceClose', (ev) => {
    const win = getWindow(ev.sender.id)!
    win.setClosable(true)
    win.removeAllListeners('close')
    win.close()
  })

  ipcMain.handle('getCurrentUser', () => getCurrentUser())

  ipcMain.handle('isOnline', () => isOnline())

  ipcMain.handle('getPath', (ev, name) => {
    return app.getPath(name)
  })
}
