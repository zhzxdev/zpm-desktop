import '@/preload'
import { app } from 'electron'
import { autoUpdater } from 'electron-updater'
import logger from 'electron-log'
import { createTray } from '@/tray'
import init from '@/modules'
import { createFloatIconWindow } from '@/wm'

async function tryUpdate() {
  try {
    const result = await autoUpdater.checkForUpdates()
    if (result.downloadPromise) {
      await result.downloadPromise
      autoUpdater.quitAndInstall(true, true)
      return true
    }
  } catch (e) {
    logger.error(`[UPDATER] Error: ${e.message}`)
  }
  return false
}

if (app.requestSingleInstanceLock()) {
  app.on('window-all-closed', () => {
    //
  })

  // https://stackoverflow.com/questions/38986692/how-do-i-trust-a-self-signed-certificate-from-an-electron-app
  // This is the self signed certificate support
  app.on('certificate-error', (event, webContents, url, error, certificate, callback) => {
    // On certificate error we disable default behaviour (stop loading the page)
    // and we then say "it is all fine - true" to the callback
    event.preventDefault()
    callback(true)
  })

  app.on('ready', () => {
    void tryUpdate().then((hasUpdate) => {
      if (hasUpdate) {
        logger.info('[MAIN] Start application update')
      } else {
        logger.info('[MAIN] Initialize application')
        createTray()
        createFloatIconWindow()

        if (process.env.NODE_ENV !== 'development') {
          app.setLoginItemSettings({ openAtLogin: true })
        }

        void init()
      }
    })
  })
} else {
  app.quit()
}
