import path from 'path'
import { readFile } from 'fs-extra'
import { listDrives, wait } from '@/misc/utils'
import { verify } from '@/modules/api'
import { createWindow } from '@/wm'

const TOKEN_SCANNER_INTERVAL = 1000

let currentUser: any = null
let lastToken: string | undefined

async function scanToken() {
  const drives = await listDrives()
  drives.sort().reverse()
  try {
    let token: string | undefined = undefined
    for (const drive of drives) {
      try {
        const file = await readFile(path.join(drive, '.cfg', '.zpm_token'))
        token = file.toString()
        break
      } catch (e) {
        //
      }
      try {
        const file = await readFile(path.join(drive, '.cfg', 'zhzx-print-station'))
        token = file.toString()
        break
      } catch (e) {
        //
      }
    }
    return token
  } catch (e) {
    return
  }
}

async function verifyUser(token: string) {
  try {
    const user = await verify(token)
    return user
  } catch (e) {
    return
  }
}

export default async function (): Promise<void> {
  for (;;) {
    const token = await scanToken()
    if (token === lastToken) continue
    lastToken = token
    if (token) {
      // User come
      const user = await verifyUser(token)
      if (user) {
        currentUser = user
        createWindow()
      }
    } else {
      // User left
      currentUser = null
    }
    await wait(TOKEN_SCANNER_INTERVAL)
  }
}

export function getCurrentUser(): any {
  return currentUser
}
