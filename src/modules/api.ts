import logger from 'electron-log'
import gotFac from 'got'
import { wait } from '@/misc/utils'

const got = gotFac.extend({
  prefixUrl: ZPM_SERVER,
  responseType: 'json',
  https: {
    rejectUnauthorized: false
  }
})

export async function ping(): Promise<any> {
  const { body } = await got.get<any>('api/device/ping')
  return body
}

export async function verify(token: string): Promise<any> {
  const { body } = await got.get<any>('api/device/verify', { searchParams: { token } })
  return body
}

export default async function (): Promise<void> {
  for (;;) {
    try {
      const { device, server } = await ping()
      logger.log(`[API] server ${server.version} device ${device.name}`)
      break
    } catch (e) {
      logger.error(`[API] Error ${e.message}`)
      await wait(1000)
    }
  }
}
